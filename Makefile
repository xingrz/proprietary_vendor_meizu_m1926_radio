DEVICE := m1926

SDAT_SUFFIXES = .new.dat.br .patch.dat .transfer.list

MODEM_IMG := firmware-update/NON-HLOS.bin
VBMETA_IMG := firmware-update/vbmeta.img
VENDOR_SDAT := $(addprefix vendor,$(SDAT_SUFFIXES))

TIMESTAMP := $(shell strings $(MODEM_IMG) | grep -m 1 '"Time_Stamp"' | sed -n 's|.*"Time_Stamp": "\([^"]*\)"|\1|p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's|[ :-]*||g')

HASH_VBMETA := $(shell openssl dgst -r -sha1 $(VBMETA_IMG) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update $(VENDOR_SDAT)
	zip -r0 $@ $(filter %.new.dat.br,$^)
	zip -r9 $@ $(filter-out %.new.dat.br,$^)

vendor.new.dat.br: vendor.new.dat
	brotli -o $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(VBMETA_IMG)
ifneq ($(HASH_VBMETA), 1f31d0183c93efaee09277443aab1e4c2a1e0209)
	$(error SHA-1 of vbmeta.img mismatch)
endif
	@echo Everything is ok.

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMG)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
